package common

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/nikita.morozov/ms-shared/models"
	"net/http"
)

func MsErrorHandler(err error, c echo.Context) {
	var response models.Error

	if r, ok := err.(*models.NotFoundError); ok {
		response = models.Error{
			IsPublic: true,
			Message:  r.Error(),
		}

		c.Logger().Errorf("Not found error: %s | %s", r.Error(), c.Path())
		c.JSON(404, response)

		return
	}

	code := http.StatusBadRequest
	if r, ok := err.(*models.PublicError); ok {
		response = models.Error{
			IsPublic: true,
			Message:  r.Error(),
		}
		c.Logger().Errorf("Public error: %s | %s", r.Error(), c.Path())
	} else {
		if he, ok := err.(*echo.HTTPError); ok {
			if he.Code != http.StatusOK {
				code = he.Code
				response = models.Error{
					Message: he.Message.(string),
				}

				c.Logger().Errorf("Error handler: %d | %s | %s", code, he.Message.(string), c.Path())
			}
		} else {
			response = models.Error{
				Message: err.Error(),
			}

			c.Logger().Errorf("Error handler: %d | %s | %s", code, err.Error(), c.Path())
		}
	}

	c.JSON(code, response)
}
