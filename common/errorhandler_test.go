package common

import (
	"errors"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMsErrorHandler(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	MsErrorHandler(errors.New("alarm.error"), c)

	assert.True(t, true)
	assert.Equal(t, rec.Code, 500)
	assert.Equal(t, rec.Body.String(), "{\"message\":\"alarm.error\"}\n")
}
