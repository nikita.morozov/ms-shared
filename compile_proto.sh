#!/bin/bash

protoc --go_out=./ --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    ./proto/commonTypes.proto

protoc --go_out=./ --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    ./proto/paginatior.proto