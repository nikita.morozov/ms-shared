package delivery

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

type commonHttpHandler struct {
	e *echo.Echo
}

func (h *commonHttpHandler) Health(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}

func (h *commonHttpHandler) Routes(c echo.Context) error {
	return c.JSON(http.StatusOK, h.e.Routes())
}

func NewCommonHttpHandler(e *echo.Echo, useRoutes bool) {
	handler := commonHttpHandler{
		e: e,
	}

	e.GET("/api/health", handler.Health)

	if useRoutes {
		e.GET("/api/routes", handler.Routes)
	}
}
