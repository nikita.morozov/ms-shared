package middleware

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"regexp"
)

type contextMiddleware struct {
	Items    []string
	Contexts []string
}

type ContextMiddleware interface {
	Add(desc grpc.ServiceDesc, method string)
}

type CheckContext interface {
	ContextMiddleware
	NeedCheck(item string) bool
	Interceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error)
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func (a *contextMiddleware) Add(desc grpc.ServiceDesc, method string) {
	fullMethod := fmt.Sprintf("/%s/%s", desc.ServiceName, method)
	if contains(a.Items, fullMethod) {
		log.Fatal(fmt.Sprintf("Method %s already added", fullMethod))
	}
	exist := false
	for _, v := range desc.Methods {
		if v.MethodName == method {
			exist = true
		}
	}

	if !exist {
		log.Fatal(fmt.Sprintf("Method %s not found in current descriptor", fullMethod))
	}

	a.Items = append(a.Items, fullMethod)
}

func (a *contextMiddleware) NeedCheck(item string) bool {
	return contains(a.Items, item)
}

func getValueFromContext(md metadata.MD, key string) string {
	r := regexp.MustCompile("\\S+")

	if len(md[key]) == 0 {
		return ""
	}

	res := r.FindAllString(md[key][0], -1)
	if len(res[0]) == 0 {
		return ""
	}

	return res[0]
}

func (a *contextMiddleware) Interceptor(ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (interface{}, error) {

	if a.NeedCheck(info.FullMethod) {
		md, ok := metadata.FromIncomingContext(ctx)
		if ok {
			for _, v := range a.Contexts {
				ctx = context.WithValue(ctx, v, getValueFromContext(md, v))
			}
		}
	}

	// Calls the handler
	h, err := handler(ctx, req)

	return h, err
}

func NewContextMiddleware(contexts []string) CheckContext {
	return &contextMiddleware{
		Items:    []string{},
		Contexts: contexts,
	}
}
