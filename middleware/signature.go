package middleware

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
)

func CheckSignature(secret string) echo.MiddlewareFunc {
	return func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			signatureHeader := c.Request().Header.Get("x-hook-signature")

			if len(signatureHeader) == 0 {
				return errors.New("common.signature.absence")
			}

			signature := sha1.New()
			signature.Write([]byte(secret))

			_signature := fmt.Sprintf("%x", signature.Sum(nil))

			println(_signature)

			if fmt.Sprintf("%x", signature.Sum(nil)) != signatureHeader {
				return errors.New("common.signature.not.verified")
			}

			return h(c)
		}
	}
}

func NewCheckSignature(secret string) echo.MiddlewareFunc {
	return CheckSignature(secret)
}
