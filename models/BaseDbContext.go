package models

type DbContext struct{}

type IDbContext interface {
	Create()
	Commit()
	Rollback()
}
