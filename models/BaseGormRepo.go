package models

import (
	"gorm.io/gorm"
)

type BaseGormRepo struct {
	DB *gorm.DB
}

func (r *BaseGormRepo) GetDb(c interface{}) *gorm.DB {
	switch v := c.(type) {
	case GormDbContext:
		return v.GetDb()
	default:
		return r.DB
	}
}
