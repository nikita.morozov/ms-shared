package models

import "gorm.io/gorm"

type GormDbContext struct {
	DbContext
	db *gorm.DB
	tx *gorm.DB
}

func (g *GormDbContext) Create() {
	g.tx = g.db.Begin()
}

func (g *GormDbContext) Commit() {
	if g.tx != nil {
		g.tx.Commit()
	}
}

func (g *GormDbContext) Rollback() {
	if g.tx != nil {
		g.tx.Rollback()
	}
}

func (g *GormDbContext) GetDb() *gorm.DB {
	if g.tx != nil {
		return g.tx
	}

	return g.db
}

func NewGormContext(db *gorm.DB) IDbContext {
	return &GormDbContext{}
}
