package models

type Error struct {
	IsPublic bool   `json:"__public"`
	Message  string `json:"message"`
}
