package models

type NotFoundError struct {
	msg string
}

func (m *NotFoundError) Error() string {
	return m.msg
}

func NewNotFoundError(err string) error {
	return &NotFoundError{
		msg: err,
	}
}
