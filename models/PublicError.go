package models

type PublicError struct {
	msg string
}

func (m *PublicError) Error() string {
	return m.msg
}

func NewPublicError(err string) error {
	return &PublicError{
		msg: err,
	}
}
