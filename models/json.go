package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

type JSONBUint []uint

func (j JSONBUint) Value() (driver.Value, error) {
	if len(j) == 0 {
		return "[]", nil
	}

	valueString, err := json.Marshal(j)
	return valueString, err
}

func (j *JSONBUint) Scan(value interface{}) error {
	var err error
	switch v := value.(type) {
	case []byte:
		err = json.Unmarshal(v, &j)
	case string:
		err = json.Unmarshal([]byte(v), &j)
	default:
		err = errors.New("jsonb.scan.type-value-is-wrong")
	}
	return err
}

type JSONBUint64 []uint64

func (j JSONBUint64) Value() (driver.Value, error) {
	if len(j) == 0 {
		return "[]", nil
	}

	valueString, err := json.Marshal(j)
	return valueString, err
}

func (j *JSONBUint64) Scan(value interface{}) error {
	var err error
	switch v := value.(type) {
	case []byte:
		err = json.Unmarshal(v, &j)
	case string:
		err = json.Unmarshal([]byte(v), &j)
	default:
		err = errors.New("jsonb.scan.type-value-is-wrong")
	}
	return err
}

type JSONB map[string]interface{}

func (j JSONB) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *JSONB) Scan(value interface{}) error {
	var err error
	switch v := value.(type) {
	case []byte:
		err = json.Unmarshal(v, &j)
	case string:
		err = json.Unmarshal([]byte(v), &j)
	default:
		err = errors.New("jsonb.scan.type-value-is-wrong")
	}
	return err
}

type JSONBString []string

func (j JSONBString) Value() (driver.Value, error) {
	valueString, err := json.Marshal(j)
	return string(valueString), err
}

func (j *JSONBString) Scan(value interface{}) error {
	var err error
	switch v := value.(type) {
	case []byte:
		err = json.Unmarshal(v, &j)
	case string:
		err = json.Unmarshal([]byte(v), &j)
	default:
		err = errors.New("jsonb.scan.type-value-is-wrong")
	}
	return err
}
