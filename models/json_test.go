package models

import (
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"testing"
)

type JSONSuite struct {
	suite.Suite
}

func TestJSON(t *testing.T) {
	suite.Run(t, new(JSONSuite))
}

func (s *JSONSuite) Test_String() {
	dataString := "{\"field\":\"data\"}"

	var data JSONB
	err := data.Scan(dataString)

	require.NoError(s.T(), err)
}

func (s *JSONSuite) Test_ByteArray() {
	dataString := []byte("{\"field\":\"data\"}")

	var data JSONB
	err := data.Scan(dataString)

	require.NoError(s.T(), err)
}

func (s *JSONSuite) Test_Int() {
	dataString := "{\"field\":1}"

	var data JSONB
	err := data.Scan(dataString)

	require.NoError(s.T(), err)
}
