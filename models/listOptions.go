package models

import (
	"bytes"
	"fmt"
	"strings"
)

type ListOptions struct {
	Order  *string `json:"order"`
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
}

func (r *ListOptions) GetOrder() string {
	var buffer bytes.Buffer
	var order = "id desc"
	if r.Order != nil {
		order = *r.Order

		sections := strings.Split(order, ",")
		for _, i := range sections {
			if buffer.Len() > 0 {
				buffer.WriteString(", ")
			}

			item := strings.TrimSpace(i)
			items := strings.Split(item, " ")
			if len(items) > 2 {
				return "id desc"
			}

			if len(items) == 2 {
				key := strings.ToLower(items[0])
				way := strings.ToLower(items[1])
				if way != "asc" && way != "desc" {
					if key == "asc" || key == "desc" {
						buffer.WriteString(fmt.Sprintf("%s %s", way, key))
						continue
					}

					return "id desc"
				}
			}

			buffer.WriteString(item)
		}

		return buffer.String()
	}

	return order
}

func (r *ListOptions) GetLimit(defaultValue int) int {
	value := r.Limit
	if r.Limit == 0 {
		value = defaultValue
	}

	return value
}
