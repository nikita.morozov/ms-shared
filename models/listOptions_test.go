package models

import (
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"testing"
)

type ListOptionsTest struct {
	suite.Suite
}

func TestListOptLimit(t *testing.T) {
	suite.Run(t, new(ListOptionsTest))
}

const defaultValue = 10

func (s *ListOptionsTest) Test_LimitTest() {
	opts := ListOptions{
		Limit: 1,
	}

	require.Equal(s.T(), opts.GetLimit(defaultValue), 1)
}

func (s *ListOptionsTest) Test_LimitTestWithZero() {
	opts := ListOptions{}

	require.Equal(s.T(), opts.GetLimit(defaultValue), 10)
}

func (s *ListOptionsTest) Test_LimitTestWithValue() {
	opts := ListOptions{
		Limit: 15,
	}

	require.Equal(s.T(), opts.GetLimit(defaultValue), 15)
}

func (s *ListOptionsTest) Test_GetOrder() {
	order := "rate desc, created"
	opts := ListOptions{
		Order: &order,
	}

	require.Equal(s.T(), "rate desc, created", opts.GetOrder())
}

func (s *ListOptionsTest) Test_GetOrderReverse() {
	order := "desc sort"
	opts := ListOptions{
		Order: &order,
	}

	require.Equal(s.T(), "sort desc", opts.GetOrder())
}

func (s *ListOptionsTest) Test_GetOrderReverseSecond() {
	order := "desc id, created"
	opts := ListOptions{
		Order: &order,
	}

	require.Equal(s.T(), "id desc, created", opts.GetOrder())
}

func (s *ListOptionsTest) Test_GetOrder1() {
	order := "rate desc, DROP TABLE table_name;"
	opts := ListOptions{
		Order: &order,
	}

	require.Equal(s.T(), "id desc", opts.GetOrder())
}

func (s *ListOptionsTest) Test_GetOrder2() {
	order := "DROP TABLE table_name"
	opts := ListOptions{
		Order: &order,
	}

	require.Equal(s.T(), "id desc", opts.GetOrder())
}
