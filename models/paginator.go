package models

type Paginator struct {
	Offset       int `json:"offset"`
	TotalCount   int `json:"totalCount"`
	CountPerPage int `json:"countPerPage"`
}
