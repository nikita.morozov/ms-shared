package models

import (
	"errors"
	"sync"
)

type Registry interface {
	Add(key string, instance interface{})
	Get(key string) (interface{}, error)
	List() map[string]interface{}
}

type registry struct {
	sync.Mutex
	handler string
	items   map[string]interface{}
}

func (h *registry) List() map[string]interface{} {
	return h.items
}

func (h *registry) Add(key string, instance interface{}) {
	h.Lock()
	h.items[key] = instance
	h.Unlock()
}

func (h *registry) Get(key string) (interface{}, error) {
	h.Lock()
	defer h.Unlock()
	if val, ok := h.items[key]; ok {
		return val, nil
	}

	return nil, errors.New("handler-registry.item.not-found")
}

func NewRegistry() Registry {
	return &registry{
		items: make(map[string]interface{}),
	}
}
