package response

type ItemResp struct {
	Item interface{} `json:"item"`
}
