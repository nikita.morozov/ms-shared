package response

import "gitlab.com/nikita.morozov/ms-shared/models"

type ListPaginatorResp struct {
	List      interface{}      `json:"list"`
	Paginator models.Paginator `json:"paginator"`
}
