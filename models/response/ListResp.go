package response

type ListResp struct {
	List interface{} `json:"list"`
}
