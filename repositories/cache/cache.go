package cache

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"os"
)

type cacheRepository struct {
	cachePath string
}

type CacheRepo interface {
	Get(fileName string) ([]byte, error)
	Save(fileName string, data []byte) error
}

func (c cacheRepository) Get(fileName string) ([]byte, error) {
	shasum := sha1.New()

	fileUrl := fmt.Sprintf("%s/%s", c.cachePath, fileName)

	shasum.Write([]byte(fileUrl))
	sha1Hash := hex.EncodeToString(shasum.Sum(nil))

	return os.ReadFile(fmt.Sprintf("%s/%s", c.cachePath, sha1Hash))
}

func (c cacheRepository) Save(fileName string, data []byte) error {
	if _, err := os.Stat(c.cachePath); err != nil {
		err := os.MkdirAll(c.cachePath, os.ModePerm)
		if err != nil {
			return err
		}
	}

	shasum := sha1.New()

	fileUrl := fmt.Sprintf("%s/%s", c.cachePath, fileName)

	shasum.Write([]byte(fileUrl))
	sha1Hash := hex.EncodeToString(shasum.Sum(nil))

	return os.WriteFile(fmt.Sprintf("%s/%s", c.cachePath, sha1Hash), data, 0644)
}

func NewCacheRepository(cachePath string) CacheRepo {
	return &cacheRepository{
		cachePath: cachePath,
	}
}
