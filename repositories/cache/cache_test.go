package cache

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

type File struct {
	Width  int64
	Height int64
	Name   string
}

func TestCache(t *testing.T) {
	cachePath := "./cache"
	repo := NewCacheRepository(cachePath)

	image := &File{
		Width:  400,
		Height: 400,
		Name:   "response.png",
	}

	shasum := sha1.New()

	fileName := fmt.Sprintf("%dx%dx%s", image.Width, image.Height, image.Name)

	shasum.Write([]byte(cachePath + "/" + fileName))
	sha1Hash := hex.EncodeToString(shasum.Sum(nil))

	err := repo.Save(fileName, []byte{})

	if _, err = os.Stat(cachePath); err == nil {
		require.Error(t, errors.New("folder has not created"))
	}

	require.NoError(t, err)

	if _, err = os.Stat(cachePath + "/" + sha1Hash); err == nil {
		require.Error(t, errors.New("file has not created"))
	}

	require.NoError(t, err)

	data, _ := repo.Get(fmt.Sprintf("%dx%dx%s", image.Width, image.Height, image.Name))

	require.Equal(t, data, []byte{})
	os.RemoveAll(cachePath)
}
