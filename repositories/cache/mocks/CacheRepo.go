// Code generated by mockery 2.9.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// CacheRepo is an autogenerated mock type for the CacheRepo type
type CacheRepo struct {
	mock.Mock
}

// Get provides a mock function with given fields: fileName
func (_m *CacheRepo) Get(fileName string) ([]byte, error) {
	ret := _m.Called(fileName)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(string) []byte); ok {
		r0 = rf(fileName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(fileName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Save provides a mock function with given fields: fileName, data
func (_m *CacheRepo) Save(fileName string, data []byte) error {
	ret := _m.Called(fileName, data)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, []byte) error); ok {
		r0 = rf(fileName, data)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
