package tools

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
)

type ConnectionOpts struct {
	Sert string
	Key  string
}

func CreateConnection(address string, desc *grpc.ServiceDesc, opts *ConnectionOpts) *grpc.ClientConn {
	var conn *grpc.ClientConn
	var err error
	if opts != nil {
		creds, err := credentials.NewServerTLSFromFile(opts.Sert, opts.Key)

		if err != nil {
			log.Fatal(fmt.Sprintf(err.Error()))
		}

		conn, err = grpc.Dial(address, grpc.WithTransportCredentials(creds))
	} else {
		conn, err = grpc.Dial(address, grpc.WithInsecure())
	}

	if err != nil {
		log.Fatal(fmt.Sprintf("Can't connect to %s microservice. Error: %s", desc.ServiceName, err))
	}
	//defer conn.Close()

	return conn
}
