package convertor

import "gitlab.com/nikita.morozov/ms-shared/proto"

func Id64(id uint64) *proto.Id64Request {
	return &proto.Id64Request{Id: id}
}
