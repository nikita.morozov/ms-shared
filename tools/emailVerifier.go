/*
 * Nikita S Morozov. Copyright (c) 2021.
 */

package tools

import (
	"encoding/json"
	"time"
)

type EmailVerifier interface {
	CreateToken(userId uint64) (string, error)
	Verify(token string) (bool, *uint64)
}

type emailVerifierItem struct {
	Gen    int64  `json:"gen"`
	UserId uint64 `json:"user_id"`
}

type emailVerifier struct {
	secure   Secure
	duration time.Duration
}

func (e *emailVerifier) CreateToken(userId uint64) (string, error) {
	item := emailVerifierItem{
		Gen:    time.Now().Add(e.duration).Unix(),
		UserId: userId,
	}
	val, err := json.Marshal(item)

	if err != nil {
		return "", err
	}

	return e.secure.Encrypt(string(val))
}

func (e *emailVerifier) Verify(token string) (bool, *uint64) {
	val, err := e.secure.Decrypt(token)
	if err != nil {
		return false, nil
	}

	var item emailVerifierItem
	err = json.Unmarshal([]byte(val), &item)
	if err != nil {
		return false, nil
	}

	if item.Gen < time.Now().Unix() {
		return false, &item.UserId
	}

	return true, &item.UserId
}

func NewEmailVerifier(key string, duration time.Duration) EmailVerifier {
	return &emailVerifier{
		secure:   NewSecure(key),
		duration: duration,
	}
}
