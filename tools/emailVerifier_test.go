package tools

import (
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestEmailVerifier(t *testing.T) {
	verifier := NewEmailVerifier("abcdefghijklmnopqrstuvwxyz012345", time.Duration(30)*time.Minute)
	token, err := verifier.CreateToken(64)
	require.NoError(t, err)
	require.True(t, len(token) > 0)

	verify, userId := verifier.Verify(token)
	require.True(t, verify)
	require.Equal(t, *userId, uint64(64))
}

func TestEmailVerifier_Expired(t *testing.T) {
	verifier := NewEmailVerifier("abcdefghijklmnopqrstuvwxyz012345", time.Duration(-30)*time.Minute)
	token, err := verifier.CreateToken(64)
	require.NoError(t, err)
	require.True(t, len(token) > 0)

	verify, userId := verifier.Verify(token)
	require.False(t, verify)
	require.NotNil(t, userId)
}

func TestEmailVerifier_BadToken(t *testing.T) {
	verifier := NewEmailVerifier("abcdefghijklmnopqrstuvwxyz012345", time.Duration(30)*time.Minute)
	token, err := verifier.CreateToken(64)
	require.NoError(t, err)
	require.True(t, len(token) > 0)

	verify, userId := verifier.Verify("a" + token)
	require.False(t, verify)
	require.Nil(t, userId)
}
