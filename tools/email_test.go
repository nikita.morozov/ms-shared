package tools

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEmail_IsEmailValid(t *testing.T) {
	assert.False(t, IsEmailValid("lia93094"))
	assert.False(t, IsEmailValid("@lia93094"))
	assert.False(t, IsEmailValid("lia93094@gmail"))
	assert.True(t, IsEmailValid("a@a.a"))
	assert.True(t, IsEmailValid("test@test.test"))
	assert.True(t, IsEmailValid("test@test.co.in"))
	assert.False(t, IsEmailValid("test@test.co.in."))
}
