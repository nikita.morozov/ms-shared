package tools

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestEncryption(t *testing.T) {
	s := NewSecure("abcdefghijklmnopqrstuvwxyz012345")
	res, err := s.Encrypt("test values")

	require.NoError(t, err)
	require.True(t, len(res) > 0)
	require.NotEqual(t, res, "test values")

	res2, err := s.Decrypt(res)
	require.NoError(t, err)
	require.Equal(t, res2, "test values")
}
