package tools

type HandlerRegistry interface {
	Add(method string, handler interface{})
	Get(method string) interface{}
}

type handlerRegistry struct {
	items map[string]interface{}
}

func (h *handlerRegistry) Add(method string, handler interface{}) {
	h.items[method] = handler
}

func (h *handlerRegistry) Get(method string) interface{} {
	return h.items[method]
}

func NewHandlerRegistry() HandlerRegistry {
	return &handlerRegistry{
		items: make(map[string]interface{}),
	}
}
