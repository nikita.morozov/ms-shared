package tools

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type TestSender interface {
	Send(string, int) (string, error)
}

type testSender struct {
	test string
}

func (s *testSender) Send(string, int) (string, error) {
	return s.test, nil
}

func NewTestSender() TestSender {
	return &testSender{
		test: "test",
	}
}

func Test_HandlerRegistry(t *testing.T) {
	handler := NewHandlerRegistry()

	sender := NewTestSender()

	handler.Add("test", sender)

	_handler := handler.Get("test")

	text, err := (_handler).(TestSender).Send("test", 1)

	require.NoError(t, err)
	require.Equal(t, text, "test")
}
