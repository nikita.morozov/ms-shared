package tools

import (
	"bytes"
	"fmt"
	"os"
)

const logo = `                                                                                
       @@@@@@@@@@@@@@@        @@@@@@    
    @@@@@@@@      @@@@@@     @@@@@@     
   @@@@@@           @@@@@@   @@@@@      
  @@@@@@              @@@@@ @@@@@@      
 @@@@@@                @@@@@@@@@@       
 @@@@@@                 @@@@@@@@        
 @@@@@@                 @@@@@@@@        
 @@@@@@                  @@@@@@         
  @@@@@@                @@@@@@@         
  @@@@@@@              @@@@@@@@@    @@@@
    @@@@@@@          @@@@@ @@@@@@@  @@@@
     @@@@@@@@@@@@@@@@@@@     @@@@@@@@@@ 
         @@@@@@@@@@@           @@@@@@   
`

func DisplayServiceName(serviceName string) {
	var version string
	if len(os.Args) >= 2 {
		version = os.Args[1]
	}

	if len(version) == 0 {
		version = os.Getenv("APP_VERSION")
	}

	const length = 80
	var size = (length - (len(serviceName) + len(version) + 4)) / 2
	buf := bytes.NewBufferString("")
	for i := 0; i < size; i++ {
		buf.WriteString("-")
	}
	fmt.Printf("%s\n%s %s, %s %s\n\n", logo, buf.String(), serviceName, version, buf.String())
}

func DisplayServiceNameWithPort(serviceName string, version string, port string) {
	const length = 80
	var size = (length - (len(serviceName) + len(version) + len(port) + 6)) / 2
	buf := bytes.NewBufferString("")
	for i := 0; i < size; i++ {
		buf.WriteString("-")
	}
	fmt.Printf("%s\n%s %s, %s, %s %s\n\n", logo, buf.String(), serviceName, version, port, buf.String())
}
