package tools

import (
	"math"
	"math/rand"
	"time"
)

func MakeId(length int) string {
	rand.Seed(time.Now().UnixNano())
	var result string
	characters := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	charactersLength := len(characters)

	for i := 0; i < length; i++ {
		result += string(characters[int64(math.Floor(rand.Float64() * float64(charactersLength)))])
	}
	return result
}
