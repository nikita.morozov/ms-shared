package tools

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestMakeId(t *testing.T) {
	result := MakeId(4)
	require.Equal(t, 4, len(result))
	result = MakeId(10)
	require.Equal(t, 10, len(result))
	result = MakeId(20)
	require.Equal(t, 20, len(result))
	result = MakeId(40)
	require.Equal(t, 40, len(result))
}
