package tools

import "encoding/json"

func RefactorModel[K any, V any](in *K) (*V, error) {
	data, err := json.Marshal(in)
	if err != nil {
		return nil, err
	}
	var item V
	err = json.Unmarshal(data, &item)

	if err != nil {
		return nil, err
	}

	return &item, err
}
